MODEL : bert-attn
GLOVE_PATH : /data/data_store/rabiul/pre-embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : /student/mda219/workspace/angrybert/resource_v2
EMB_DROPOUT : 0.5
FC_DROPOUT : 0.1
MIN_OCC : 3
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 128
PROJ_DIM : 32
NUM_HIDDEN : 200
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : True
L_RNN_DROPOUT : 0.1
TASKS : wz,hatelingo,semeval_c,emotion
LENGTH : 30
CREATE_DICT : False
CREATE_EMB : False
SAVE_NUM : 2
EPOCHS : 4
CROSS_VAL : 5
SEED : 1111
CUDA_DEVICE : 3
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 158.66
	eval task wz precision: 79.96 recall: 77.02 f1: 77.68
	eval task hatelingo precision: 89.38 recall: 93.07 f1: 91.06
	eval task semeval_c precision: 58.48 recall: 62.73 f1: 50.37
	eval task emotion roc auc score for multi label classification: 66.38 

epoch 1
	 multi task train_loss: 101.30
	eval task wz precision: 80.70 recall: 72.15 f1: 73.23
	eval task hatelingo precision: 90.22 recall: 91.14 f1: 89.94
	eval task semeval_c precision: 63.56 recall: 71.50 f1: 67.21
	eval task emotion roc auc score for multi label classification: 79.00 

epoch 2
	 multi task train_loss: 74.73
	eval task wz precision: 80.75 recall: 75.93 f1: 76.79
	eval task hatelingo precision: 87.83 recall: 87.72 f1: 86.65
	eval task semeval_c precision: 63.89 recall: 71.13 f1: 67.31
	eval task emotion roc auc score for multi label classification: 82.34 

epoch 3
	 multi task train_loss: 60.29
	eval task wz precision: 81.63 recall: 77.70 f1: 78.46
	eval task hatelingo precision: 91.02 recall: 91.40 f1: 90.06
	eval task semeval_c precision: 74.14 recall: 71.38 f1: 67.47
	eval task emotion roc auc score for multi label classification: 83.18 

