import errno
import os
import json
import numpy as np
import torch

def assert_exits(path):
    assert os.path.exists(path), 'Does not exist : {}'.format(path)
    
def equal_info(a,b):
    assert len(a)==len(b),'File info not equal!'
    
def same_question(a,b):
    assert a==b,'Not the same question!'
    
class Logger(object):
	def __init__(self,output_dir):
		dirname=os.path.dirname(output_dir)
		if not os.path.exists(dirname):
			os.mkdir(dirname)
		self.log_file=open(output_dir,'w')
		self.infos={}
		
	def append(self,key,val):
		vals=self.infos.setdefault(key,[])
		vals.append(val)

	def log(self,extra_msg=''):
		msgs=[extra_msg]
		for key, vals in self.infos.iteritems():
			msgs.append('%s %.6f' %(key,np.mean(vals)))
		msg='\n'.joint(msgs)
		self.log_file.write(msg+'\n')
		self.log_file.flush()
		self.infos={}
		return msg
		
	def write(self,msg):
		self.log_file.write(msg+'\n')
		self.log_file.flush()
		print(msg)

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def attention_dump(opt,model,test_set, idx):
    # print ("The information for task {} # iterations : {} last batch : {}".format(source, len(test_set),test_set.last_batch))
    tasks=opt.TASKS.split(',')
    total = len(test_set)
    words_list = []

    weights_info = {"0":[],"1":[],"2":[],"3":[]}
    preds_info = {"0":[],"1":[],"2":[],"3":[]}
    
    for i in range(total):
        with torch.no_grad():
            batch_info=test_set.next_batch()
            tokens=batch_info['tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            bert_tokens=batch_info['bert_tokens'].cuda()
            masks=batch_info['masks'].cuda()
            att_masks = batch_info['att_masks'].cuda()
            words=batch_info['words']
            words_list.extend(words)
            for task_idx in range(len(tasks)):
                pred,weights=model(tokens,task_idx,bert_tokens,masks,att_masks)
                weights_info[str(task_idx)].extend(weights.cpu().numpy())
                preds_info[str(task_idx)].extend(pred.cpu().numpy())
        if i==0:
            #print ('yes')
            t1_labels=labels
        else:
            t1_labels=torch.cat((t1_labels,labels),0)
    t1_labels = t1_labels.cpu().numpy()
    write_json(words_list, weights_info, preds_info, t1_labels, tasks[0],idx)


def write_json(sent_tokens, weights, preds, labels, source,cv_idx):
    items = []
    for idx in range(len(sent_tokens)):
        weights_tuple = (weights["0"][idx],weights["1"][idx],weights["2"][idx],weights["3"][idx])
        preds_tuple = (preds["0"][idx],preds["1"][idx],preds["2"][idx],preds["3"][idx])
        item = {"words": sent_tokens[idx], "weights": weights_tuple, "predictions": preds_tuple, "label": np.argmax(labels[idx])}
        items.append(item)

    with open("weights/{}.attentions.json".format(source+str(cv_idx)), "w") as f:
        json.dump(items, f, cls=NpEncoder)
