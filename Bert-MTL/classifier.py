import torch.nn as nn
from torch.nn.utils.weight_norm import weight_norm

class SimpleClassifier(nn.Module):
    def __init__(self,in_dim,hid_dim,out_dim,dropout):
        super(SimpleClassifier,self).__init__()
        layer=[
            weight_norm(nn.Linear(in_dim,hid_dim),dim=None),
            nn.ReLU(),
            nn.Dropout(dropout,inplace=True),
            weight_norm(nn.Linear(hid_dim,out_dim),dim=None)
        ]
        self.main=nn.Sequential(*layer)
        
    def forward(self,x):
        logits=self.main(x)
        return logits
    
    
class SingleClassifier(nn.Module):
    def __init__(self,in_dim,out_dim,dropout):
        super(SingleClassifier,self).__init__()
        layer=[
            weight_norm(nn.Linear(in_dim,out_dim),dim=None),
            nn.Dropout(dropout,inplace=True)
        ]
        self.main=nn.Sequential(*layer)
        
    def forward(self,x):
        logits=self.main(x)
        return logits   

class FC(nn.Module):
    def _init_(self,in_dim,out_dim,dropout):
        super(FC,self).__init__()
        self.main = nn.Linear(in_dim,out_dim)
        slef.drop = nn.Dropout(dropout)
    def forward(self, x):
        logits = self.main(x)
        logits = self.drop(logits)
        return logits
