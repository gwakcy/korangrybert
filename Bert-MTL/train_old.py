import os
import time
import torch
import torch.nn as nn
import utils
import torch.nn.functional as F
import config
import numpy as np
import h5py
import pickle as pkl
import json
import random
from torch.utils.data import DataLoader
from sklearn.utils.multiclass import type_of_target
from sklearn.metrics import f1_score,recall_score,precision_score,accuracy_score,classification_report,precision_recall_fscore_support

def log_hyperpara(logger,opt):
    dic = vars(opt)
    for k,v in dic.items():
        logger.write(k + ' : ' + str(v))

def bce_for_loss(logits,labels):
    loss=nn.functional.binary_cross_entropy_with_logits(logits, labels)
    # loss*=labels.size(1)
    #print (loss)
    return loss

def compute_score(logits,labels):
    logits=torch.max(logits,1)[1]
    labels=torch.max(labels,1)[1]
    score=logits.eq(labels)
    score=score.sum().float()
    return score

def compute_other(logits,labels):
    acc=compute_score(logits,labels)
    logits=np.argmax(logits.cpu().numpy(),axis=1)
    label=np.argmax(labels.cpu().numpy(),axis=1)
    length=logits.shape[0]

    f1=f1_score(label,logits,average='weighted',labels=np.unique(label))
    recall=recall_score(label,logits,average='weighted',labels=np.unique(label))
    precision=precision_score(label,logits,average='weighted',labels=np.unique(label))

    result=classification_report(label,logits)
    print (result)
    information=result.split('\n')
    #print(information,result)
    cur=information[2].split('     ')
    h_p=float(cur[3].strip())
    h_r=float(cur[4].strip())
    h_f=float(cur[5].strip())
    return f1,recall,precision,acc,result,h_p,h_r,h_f

class MultiTaskLoss(nn.Module):
    def __init__(self):
        super(MultiTaskLoss, self).__init__()
        self.tasks = 2
        self.log_vars = nn.Parameter(torch.zeros(self.tasks).cuda())

    def forward(self, preds, targets):
        loss_list = [bce_for_loss(x, y) for x, y in zip(preds, targets)]
        precision_0 = torch.exp(-self.log_vars[0])
        loss_0 = precision_0 * loss_list[0]
        
        precision_1 = torch.exp(-self.log_vars[1])
        loss_1 = precision_1 * loss_list[1]
        
        loss = loss_0 + loss_1
        return loss_list[0], loss_list[1], loss

def train_for_deep(opt,model,total_train,total_test):
    tasks=opt.TASKS.split(',')
    model_optim = torch.optim.Adamax(model.parameters())
    if tasks[0]=='dt':
        logger=utils.Logger(os.path.join(opt.DT,'log'+str(opt.SAVE_NUM)+'.txt'))
    elif tasks[0]=='wz':
        logger=utils.Logger(os.path.join(opt.WZ_RESULT,'log'+str(opt.SAVE_NUM)+'.txt'))
    elif tasks[0]=='founta':
        logger=utils.Logger(os.path.join(opt.FOUNTA_RESULT,'log'+str(opt.SAVE_NUM)+'.txt'))
    log_hyperpara(logger,opt)

    total_loss=[0.0 for _ in range(len(tasks))]
    """
    all tasks have the same number of training epochs
    now for evaluating on other tasks, I just wanna to 
    """
    eval_iters=[total_train[i].num_iters for i in range(len(tasks))]
    total_iters=[opt.EPOCHS *total_train[i].num_iters for i in range(len(tasks))]
    max_iters=sum(total_iters)
    cur_iters=[0 for _ in range(len(tasks))]
    #task to task index
    cur_tasks={task:i for i,task in enumerate(tasks)}
    for i in range(len(tasks)):
        print (tasks[i],eval_iters[i],total_iters[i])
    for iters in range(max_iters):
        """
        choose a task among the remaining tasks
        """
        if iters % 200 ==0:
            print ('Number of remaining tasks:',len(cur_tasks))
            for name in cur_tasks.keys():
                idx=cur_tasks[name]
                print (idx,name,total_iters[idx],cur_iters[idx])
        choices=[cur_tasks[name] for name in cur_tasks.keys()]
        task_idx=random.choice(choices)
        
        batch_info=total_train[task_idx].next_batch()
        tokens=batch_info['tokens'].cuda()
        labels=batch_info['label'].float().cuda()
        pred=model(tokens,task_idx)
        loss=bce_for_loss(pred,labels)
        total_loss[task_idx]+=loss
        model_optim.zero_grad()
        loss.backward()
        model_optim.step()
        cur_iters[task_idx]+=1
        if cur_iters[task_idx]>=total_iters[task_idx]:
            print (tasks[task_idx])
            cur_tasks.pop(tasks[task_idx])

        """
        currently I don't want to record all results but hate speech
        since it is trained randomly, the evaluation will go through randomly
        in this way, the recording will be messing
        """
        if (cur_iters[task_idx] % eval_iters[task_idx])==0:
            print ('Starting to evaluate the model...',iters)
            logger.write('epoch %d' %(cur_iters[task_idx] / eval_iters[task_idx]))
            logger.write('\t task %s train_loss: %.2f' % (tasks[task_idx], total_loss[task_idx]))
            total_loss[task_idx]=0.0
            #re-initialize the records for loss
            
            #for basic MTL, roc is the F1 for the lingo task
            if (tasks[task_idx] in ['wz','dt','founta']):
                model.train(False)
                e_f1,e_recall,e_precision,h_f1,h_recall,h_precision=evaluate_for_offensive(opt,model,total_test[task_idx])
                logger.write('\teval task %s precision: %.2f ' % (tasks[task_idx], e_precision))
                logger.write('\teval task %s recall: %.2f ' % (tasks[task_idx], e_recall))
                logger.write('\teval task %s f1: %.2f ' % (tasks[task_idx], e_f1))
                logger.write('\teval task %s hate precision: %.2f ' % (tasks[task_idx], h_precision))
                logger.write('\teval task %s hate recall: %.2f ' % (tasks[task_idx], h_recall))
                logger.write('\teval task %s hate f1: %.2f ' % (tasks[task_idx], h_f1))
                model.train(True)
    return e_f1,e_recall,e_precision,h_f1,h_recall,h_precision

def evaluate_for_offensive(opt,model,test_set):
    print ('The information for task 1 iterations is:',len(test_set),test_set.last_batch)
    total = len(test_set)
    for i in range(total):
        with torch.no_grad():
            batch_info=test_set.next_batch()
            tokens=batch_info['tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            pred=model(tokens)

        if i==0:
            #print ('yes')
            t1_labels=labels
            t1_pred=pred
        else:
            t1_labels=torch.cat((t1_labels,labels),0)
            t1_pred=torch.cat((t1_pred,pred),0)
    f1,recall,precision,acc,result, h_precision,h_recall,h_f1=compute_other(t1_pred,t1_labels)

    return 100*f1,100*recall,100*precision,100*h_f1,100*h_recall,100*h_precision


