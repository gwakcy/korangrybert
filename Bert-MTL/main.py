import torch
import torch.nn as nn
import numpy as np

from dataset import Base_Op,Wraped_Data
import baseline
from train import train_for_deep
import utils
import config
import os
import pickle as pkl

if __name__=='__main__':
    opt= config.parse_opt()
    torch.cuda.set_device(opt.CUDA_DEVICE)
    torch.manual_seed(opt.SEED)
    
    tasks=opt.TASKS.split(',')
    hate_dataset=tasks[0]
    #result saving
    if hate_dataset=='wz':
        logger=utils.Logger(os.path.join(opt.WZ_RESULT,'final_'+str(opt.SAVE_NUM)+'.txt'))
    elif hate_dataset=='dt':
        logger=utils.Logger(os.path.join(opt.DT,'final_'+str(opt.SAVE_NUM)+'.txt'))
    elif hate_dataset=='founta':
        logger=utils.Logger(os.path.join(opt.FOUNTA_RESULT,'final_'+str(opt.SAVE_NUM)+'.txt'))
    
    dictionary=Base_Op()
    dictionary.init_dict()
    hate_dataset=pkl.load(open(os.path.join(opt.SPLIT_DATASET,hate_dataset+'.pkl'),'rb'))
    
    constructor='build_baseline'
    #definitions for criteria
    
    """
    recording for both total F1 and hate F1
    change to dynamic number of tasks
    """
    total=[]
    for i in range(opt.CROSS_VAL):
        """
        construct a list
        the first position of the list is the hate speech detection dataset
        """
        total_train=[]
        total_test=[]
        for task_idx,task in enumerate(tasks):
            print (task_idx,task)
            dataset=pkl.load(open(os.path.join(opt.SPLIT_DATASET,task)+'.pkl','rb'))
            train_set=Wraped_Data(opt,dictionary,dataset,i, source=task)
            test_set=Wraped_Data(opt,dictionary,dataset,i,'test', source=task)
            total_train.append(train_set)
            total_test.append(test_set)
            if task_idx == 0:
                val_set=Wraped_Data(opt,dictionary,dataset,opt.CROSS_VAL,'val', source=task)
        model=getattr(baseline,constructor)(total_test[0],opt)   
        model=model.cuda()
        model.w_emb.init_embedding()
        #cur_total is the result for each task
        cur_total=train_for_deep(opt,model,total_train,total_test,val_set)
        
        total.append(cur_total)

        """
        please don't believe in it for WZ
        since WZ has two hate classes
        this is adaptive for DT and FOUNTA
        """
        logger.write('validation folder %d' %(i+1))
        for j,task in enumerate(tasks):
            if task in ['dt','founta']:
                logger.write('\teval task %s precision: %.2f ' % (task, cur_total[j][0]))
                logger.write('\teval task %s recall: %.2f ' % (task, cur_total[j][1]))
                logger.write('\teval task %s f1: %.2f ' % (task, cur_total[j][2]))
                logger.write('\teval task %s hate precision: %.2f ' % (task, cur_total[j][3]))
                logger.write('\teval task %s hate recall: %.2f ' % (task,  cur_total[j][4]))
                logger.write('\teval task %s hate f1: %.2f \n' % (task,  cur_total[j][5]))
            elif task == 'semeval_a':
                logger.write('\teval task %s roc auc score for multi label classification: %.2f \n' % (task,  cur_total[j][0]))
            else:
                logger.write('\teval task %s precision: %.2f ' % (task, cur_total[j][0]))
                logger.write('\teval task %s recall: %.2f ' % (task, cur_total[j][1]))
                logger.write('\teval task %s f1: %.2f \n' % (task, cur_total[j][2]))
    
    total=np.sum(total,axis=0)

    logger.write('\n final result')
    
    for j,task in enumerate(tasks):
        if task in ['dt','founta']:
            logger.write('\teval task %s precision: %.2f ' % (task,total[j][0]/opt.CROSS_VAL))
            logger.write('\teval task %s recall: %.2f ' % (task, total[j][1]/opt.CROSS_VAL))
            logger.write('\teval task %s f1: %.2f ' % (task, total[j][2]/opt.CROSS_VAL))
            logger.write('\teval task %s hate precision: %.2f ' % (task, total[j][3]/opt.CROSS_VAL))
            logger.write('\teval task %s hate recall: %.2f ' % (task, total[j][4]/opt.CROSS_VAL))
            logger.write('\teval task %s hate f1: %.2f \n' % (task, total[j][5]/opt.CROSS_VAL))
        elif task  == 'semeval_a':
            logger.write('\teval task %s roc auc score for multi label classification: %.2f \n' % (task, total[j][0]/opt.CROSS_VAL))
        else:
            logger.write('\teval task %s precision: %.2f ' % (task,total[j][0]/opt.CROSS_VAL))
            logger.write('\teval task %s recall: %.2f ' % (task, total[j][1]/opt.CROSS_VAL))
            logger.write('\teval task %s f1: %.2f ' % (task, total[j][2]/opt.CROSS_VAL))
    exit(0)
    
