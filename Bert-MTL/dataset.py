import os
import pandas as pd
import re
import json
import pickle as pkl
import numpy as np
import h5py
import torch
from torch.utils.data import Dataset
import utils
from tqdm import tqdm
import config
import itertools
import random
import math
import string
from preprocessing import clean_text

def load_pkl(path):
    data=pkl.load(open(path,'rb'))
    return data

def read_hdf5(path):
    data=h5py.File(path,'rb')
    return data

def read_csv(path):
    data=pd.read_csv(path)
    return data

def read_csv_sep(path):
    data=pd.read_csv(path,sep='\t')
    return data

def dump_pkl(path,info):
    pkl.dump(info,open(path,'wb'))  

def read_json(path):
    utils.assert_exits(path)
    data=json.load(open(path,'rb'))
    '''in anet-qa returns a list'''
    return data

def pd_pkl(path):
    data=pd.read_pickle(path)
    return data

def read_jsonl(path):
    total_info=[]
    with open(path,'rb')as f:
        d=f.readlines()
    for i,info in enumerate(d):
        data=json.loads(info)
        total_info.append(data)
    return total_info

class Base_Op(object):
    def __init__(self):
        self.opt= config.parse_opt()
        self.tasks=self.opt.TASKS.split(',')

    def tokenize(self,x):
        #x = clean_text(x).split()
        #print (x)
        x=x.lower().split()
        #print (x)
        return x

    def get_tokens(self,sent):
        tokens=self.tokenize(sent)
        #print tokens
        token_num=[]
        for t in tokens:
            if t in self.word2idx:
                token_num.append(self.word2idx[t])
            else:
                token_num.append(self.word2idx['UNK'])
        if not token_num:
            token_num.append(self.word2idx['UNK'])
        return token_num

    def get_words(self, sent):
        tokens = self.tokenize(sent)
        # print tokens
        words = []
        for t in tokens:
            if t in self.word2idx:
                words.append(t)
            else:
                words.append('UNK')
        return words

    def token_sent(self):
        cur=0
        datasets=[]
        for dataset in self.tasks:
            name=os.path.join(self.opt.SPLIT_DATASET,dataset)+'.pkl'
            data=pkl.load(open(name,'rb'))
            datasets.append(data)
        print ('Total number of datasets:',len(self.tasks))
        
       
        for i in range(6):
            cur_total=[]
            for data in datasets:
                try:
                    cur_total.extend(data[str(i)])
                except Exception as e:
                    pass

            for info in cur_total:
                tweet=info['sent']
                tokens=self.tokenize(tweet)
                for t in tokens:
                    if t not in self.word_count:
                        self.word_count[t]=1
                    else:
                        self.word_count[t]+=1
            print ('Length of current sentences:',len(cur_total))
        for word in self.word_count.keys():
            if self.word_count[word]>=self.opt.MIN_OCC:
                self.word2idx[word]=cur
                self.idx2word.append(word)
                cur+=1

        if 'PAD' not in self.word2idx:
            self.idx2word.append('UNK')
            self.word2idx['UNK']=0

        if 'UNK' not in self.word2idx:
            self.idx2word.append('UNK')
            self.word2idx['UNK']=len(self.idx2word)-1   

        dump_pkl(os.path.join(self.tasks[0],'dictionary.pkl'),[self.word2idx,self.idx2word])

    def create_dict(self):
        self.word_count={}
        self.word2idx={}
        self.idx2word=[]
        self.token_sent()

    def create_embedding(self):

        word2emb={}
        with open(self.opt.GLOVE_PATH,'r') as f:
            entries=f.readlines()
        emb_dim=len(entries[0].split(' '))-1
        weights=np.zeros((len(self.idx2word),emb_dim),dtype=np.float32)
        for entry in entries:
            word=entry.split(' ')[0]
            word2emb[word]=np.array(list(map(float,entry.split(' ')[1:])))
        for idx,word in enumerate(self.idx2word):
            if word not in word2emb:
                continue
            weights[idx]=word2emb[word]

        np.save(os.path.join(self.tasks[0],'glove_embedding.npy'),weights)
        return weights

    def init_dict(self):
        if self.opt.CREATE_DICT:
            print ('Creating Dictionary...')
            self.create_dict()
        else:
            created_dict=load_pkl(os.path.join(self.tasks[0],'dictionary.pkl'))


            self.word2idx=created_dict[0]
            self.idx2word=created_dict[1]

        if self.opt.CREATE_EMB:
            print ('Creating Embedding...;')
            self.create_embedding()

        self.ntoken()

    def ntoken(self):
        self.ntokens=len(self.word2idx)
        print ('Number of Tokens:',self.ntokens)
        return self.ntokens


    def __len__(self):
        return len(self.word2idx)

class Wraped_Data(Base_Op):
    def __init__(self,opt,dictionary,split_data,test_num,mode='training',source='dt'):
        #hate for hate speech detection dataset and lingo for hatelingo
        super(Wraped_Data,self).__init__()
        self.opt= config.parse_opt()
        random.seed(opt.SEED)
        self.dictionary=dictionary
        self.split_data=split_data
        self.test_num=test_num
        self.mode=mode
        self.source=source
        #self.batch_size_dict = {"dt": 64, "wz": 50, "hatelingo": 28, "founta": 260}
        self.class_size_dict ={
                'wz':3,'dt':3,'founta':4,'hatelingo':5,
                'offenseval_c':3,'semeval_a':11
                }
        #self.batch_size = self.batch_size_dict.get(source, opt.BATCH_SIZE)
        self.batch_size=opt.BATCH_SIZE
        self.classes = self.class_size_dict.get(source, 2)
        #loading the data: later used for batch iteration
        self.entries=self.load_tr_val_entries()
        self.num_iters=int(math.ceil(len(self.entries) * 1.0 / self.batch_size))
        self.last_batch=len(self.entries) % self.batch_size
        #print (type(self.num_iters),type(self.last_batch))
        self.cur_iter=0
        print(mode)
        print("Task name {} Batch size {} Class size {}".format(self.source, self.batch_size, self.classes))
        print('The length of all entries is:',len(self.entries))
        print ('Information about batch loader: number of iteration:',self.num_iters,'number of last batch:',self.last_batch)

        self.length=opt.LENGTH

    def load_tr_val_entries(self):
        all_data=[]
        if self.mode=='training':
            for i in range(self.opt.CROSS_VAL):
                if i==self.test_num:
                    continue
                all_data.extend(self.split_data[str(i)])
        else:
            all_data.extend(self.split_data[str(self.test_num)])
        entries=[]
        for i,info in enumerate(all_data):
            sent=info['sent']
            label=info['label']
            bert_token=info['bert_token']
            entry={
                    'sent':sent,
                    'answer':label,
                    'bert_token':bert_token
                    }
            entries.append(entry)
        #shuffle the dataset
        random.shuffle(entries)
        return entries

    def padding_bert(self,tokens,length):
        if len(tokens)<length:
            padding=[0]*(length-len(tokens))
            tokens=tokens+padding
        else:
            tokens=tokens[:length]
        return tokens

    def padding_sent(self,tokens,length):
        if len(tokens)<length:
            padding=[0]*(length-len(tokens))
            tokens=tokens+padding
        else:
            tokens=tokens[:length]
        return tokens

    def get_masks(self,tokens,length):
        masks = [1]*(len(tokens)) + [0]*(length-len(tokens))
        masks = masks[:length]
        return masks


    def next_batch(self):
        batch_info={}
        #if self.source in ["wz","dt","founta"] and self.mode == "test":
            #print("Loading iter {} # {}".format(self.mode, self.cur_iter))
        if self.cur_iter==self.num_iters-1 :
            if self.last_batch>0:
                cur_entry=self.entries[self.cur_iter*self.batch_size:self.last_batch+self.cur_iter*self.batch_size]
                if self.mode=='training':
                    self.cur_iter=-1
                    #tmp_entry = self.entries[self.cur_iter*self.batch_size:(1+self.cur_iter)*abs(len(cur_entry)-self.batch_size)]
                    #cur_entry.extend(tmp_entry)
                    random.shuffle(self.entries)
                elif self.mode=='test' or self.mode=="val":
                    self.cur_iter=-1
                    #tmp_entry = self.entries[self.cur_iter*self.batch_size:(1+self.cur_iter)*abs(len(cur_entry)-self.batch_size)]
                    #cur_entry.extend(tmp_entry)
            elif self.last_batch==0:
                cur_entry=self.entries[self.cur_iter*self.batch_size:(1+self.cur_iter)*self.batch_size]
                if self.mode=='training': 
                    self.cur_iter=-1
                    random.shuffle(self.entries)
                elif self.mode=='test' or self.mode=="val":
                    self.cur_iter=-1
            """
            if the mode is training, then restart from the beginning
            cur_iters=0
            and shuffle the dataset again
            """
        else:
            cur_entry=self.entries[self.cur_iter*self.batch_size:(1+self.cur_iter)*self.batch_size]
        if len(cur_entry)>0:
            cur_entry = sorted(cur_entry, key=lambda k:len(k['sent'].split()), reverse=True)
            self.length = len(cur_entry[0]['sent'].split())
            batch_tokens=np.zeros([len(cur_entry),self.length],dtype=np.int64)
            batch_label=np.zeros([len(cur_entry),self.classes],dtype=np.int64)
            batch_bert=np.zeros([len(cur_entry),64],dtype=np.int64)
            batch_masks=np.zeros([len(cur_entry),64],dtype=np.int64)
            batch_att_masks = np.zeros([len(cur_entry), self.length], dtype=np.int64)
            batch_words=[]
            for k in range(len(cur_entry)):
                chunck=cur_entry[k]
                #for debuging print key of data, make sure load all data
                #print (chunck['key'],self.classes)
                #get batch tokens
                sent=chunck['sent']
                bert_tokens=chunck['bert_token']
                tokens=self.dictionary.get_tokens(sent)
                pad_tokens=self.padding_sent(tokens,self.length)
                bert_pad=self.padding_bert(bert_tokens,64)
                #print (tokens,bert_tokens)
                masks=mask=[int(num>0) for num in bert_pad]
                batch_tokens[k,:]=np.array((pad_tokens),dtype=np.int64)
                batch_bert[k,:]=np.array((bert_pad),dtype=np.int64)
                batch_masks[k,:]=np.array((mask),dtype=np.int64)
                att_masks = self.get_masks(tokens, self.length)
                batch_att_masks[k,:] = np.array((att_masks),dtype=np.int64)

                words=self.dictionary.get_words(sent)
                words = words + ['PAD']*(self.length-len(words))
                words = words[:self.length]
                batch_words.append(words)

                #get batch labels
                label=chunck['answer']
                if self.source == 'semeval_a':
                    target=np.array(label)
                else:
                    target=np.zeros((self.classes),dtype=np.float32)
                    target[label]=1.0
                batch_label[k,:]=target
            batch_tokens=torch.from_numpy(batch_tokens)
            batch_bert=torch.from_numpy(batch_bert)
            batch_label=torch.from_numpy(batch_label)
            batch_masks=torch.from_numpy(batch_masks)
            batch_att_masks=torch.from_numpy(batch_att_masks)
            batch_info['tokens']=batch_tokens
            batch_info['label']=batch_label
            batch_info['bert_tokens']=batch_bert
            batch_info['words']=batch_words
            batch_info['masks']=batch_masks
            batch_info['att_masks'] = batch_att_masks
            self.cur_iter+=1
            return batch_info

    def __len__(self):
        return self.num_iters

