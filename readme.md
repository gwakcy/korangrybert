## Dependencies:  
- Python 3.6
- numpy 1.19.0
- pandas 1.0.3
- nltk
- scikit-learn
- Pytorch 1.5.1


#### Dataset Summary

| Dataset           | #tweets | classes |
|:------------------|:----:|:---:|
|DT                 | 24K     | hateful(1,430), offensive(19,190), neither(4,163)     |
|WZ                 | 16K     | sexism(3,383), racism(1,972), neither(11,559)     |
|Founta             | 99K     | hateful(3,907), offensive(19,232), neither(53,011), spam(13,840)     |
|HateLingo          | 9K      | gender, religion, ethnicity, sexual orientation     |
|SemEval_A          | 9K      | anger, anticipation, disgust, fear, joy, love, optimism, pessimism, sadness, surprise     |
|OffensEval_C       | 4K      | individual(2,507), group(1,152), other(430)   |


You can find processed datasets on `resource` dir

## Getting Started
- You can find all the codes and scripts in `BERT-MTL` folder.
- Modify config.py for required setup.
- Our implementation of AngryBERT can be found on `baseline.py` file. 
- Default model config is set to `angrybert` but you can also find other baselines. Check details on `config.py`.
- Train and evaluate the model, use code in `BERT-MTL`:  

    ``` python main.py ``` 



To cite:
```
@inproceedings{angrybert21,
    title={AngryBERT: Joint Learning Target and Emotion for Hate Speech Detection},
    author={Awal, Md Rabiul and Cao, Rui and Lee, Roy Ka-Wei and Mitrovi{\'{c}}, Sandra},
    booktitle={25th Pacific-Asia Conference on Knowledge Discovery and Data Mining},
    year={2021},
    organization={Springer}
}
```

### Correspondence 
Rabiul AWAl (mda219@usask.ca)
