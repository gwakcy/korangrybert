"""
generating datasets in five-folder for WZ,
Semeval, Gab Hate, Harassment, Toxic Detection,
and one for emotion 
"""
import argparse 
import pickle as pkl
import pandas as pd
import random
import os
import numpy as np
import csv
from transformers import BertTokenizer

if __name__ == '__main__':
    MAX_LEN=64
    parser=argparse.ArgumentParser()
    parser.add_argument('--DATASET',type=str,default='wz')
    parser.add_argument('--NUM_FOLDER',type=int,default=10)
    args=parser.parse_args()
    tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')
    
    PATH='/home/ruicao/NLP/datasets/hate-speech'
    SAVE_PATH='/home/ruicao/NLP/datasets/ids'
    #np.set_printoptions(suppress=True,threshold=17)
    
    """
    Three tasks:
    offensive or not
    if offensive, target or not
    if target, three kinds of targets
    """
    if args.DATASET=='wz':
        """
        0-racism-1923
        1-sexism-3079
        2-neither-11033
        """
        labels=open(os.path.join(PATH,'wz/NAACL_SRW_2016.csv'),'r',encoding='ISO-8859-1').readlines()
        #constructing the relation between ids and labels
        ans_dict={}
        for row in labels:
            info=row.split(',')
            ans=info[1].strip()
            label=2
            if ans=='racism':
                label=0
            elif ans=='sexism':
                label=1
            ans_dict[str(info[0])]=label
        sexism=[]
        racism=[]
        normal=[]
        total={}    
        with open(os.path.join(PATH,'wz/zeerak_waseem_2016.csv'),'r',encoding='unicode_escape') as f:
            reader=csv.DictReader(f)
            for index, line in enumerate(reader):
                sent=line['Tweets']
                if sent is None:
                    continue
                
                idx=line['ID']
                if idx==None:
                    continue
                    print (index)
                ans=ans_dict[idx]
                ent={
                    'sent':sent,
                    'id':idx
                    }
                if ans==0:
                    racism.append(ent)
                elif ans==1:
                    sexism.append(ent)
                else:
                    normal.append(ent)
            print (index)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex))
            entries=[]
            for row in cur_rac:
                sent=row['sent']
                idx=row['id']
                entry={
                    'sent':sent,
                    'label':0,
                    'id':idx
                }
                entries.append(entry)
            for row in cur_sex:
                sent=row['sent']
                idx=row['id']
                entry={
                    'sent':sent,
                    'label':1,
                    'id':idx
                }
                entries.append(entry)
            for row in cur_normal:
                sent=row['sent']
                idx=row['id']
                entry={
                    'sent':sent,
                    'label':2,
                    'id':idx
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='founta':
        """
        0-hate-4965
        1-abusive-27150
        2-normal-53851
        3-spam-14030
        """
        labels=pd.read_csv(os.path.join(PATH,'founta/hatespeech_text_label_vote_RESTRICTED.csv'),sep='\t',header=None)
        sexism=[]
        racism=[]
        normal=[]
        spam=[]
        total={}    
        for d in labels.iterrows():
            t=d[1][0]
            ans=d[1][1].strip()
            if ans=='hateful':
                racism.append(t)
            elif ans=='abusive':
                sexism.append(t)
            elif ans=='normal':
                normal.append(t)
            elif ans=='spam':
                spam.append(t)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism),'Spam:',len(spam))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        num_spam=int(len(spam)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_spam=spam[i*num_spam:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_spam=spam[i*num_spam:(i+1)*num_spam]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex),'Spam:',len(cur_spam))
            entries=[]
            for sent in cur_rac:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_sex:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':2
                }
                entries.append(entry)
            for sent in cur_spam:
                entry={
                    'sent':sent,
                    'label':3
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='dt':
        """
        0-hate-1430
        1-offensive-19190
        2-neither-4163
        """
        labels=pd.read_csv(os.path.join(PATH,'dt/labeled_data.csv'))
        tweets=labels['tweet']
        classes=labels['class']
        racism=[]#hate
        sexism=[]#offensive
        normal=[]#neither
        total={}    
        for i,c in enumerate(classes):
            t=tweets[i]
            if c==0:
                racism.append(t)
            elif c==1:
                sexism.append(t)
            elif c==2:
                normal.append(t)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex))
            entries=[]
            for sent in cur_rac:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_sex:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':2
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))